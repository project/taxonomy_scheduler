<?php

namespace Drupal\taxonomy_scheduler\EventSubscriber;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\core_event_dispatcher\EntityHookEvents;
use Drupal\core_event_dispatcher\Event\Entity\EntityPresaveEvent;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Subscribes to the entity presave event.
 */
class TaxonomySchedulerPresaveSubscriber implements EventSubscriberInterface {

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $config;

  /**
   * System date configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $systemDateConfig;

  /**
   * CacheTagsInvalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  private CacheTagsInvalidatorInterface $cacheTagsInvalidator;

  /**
   * TaxonomySchedulerPresaveSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The module config.
   * @param \Drupal\Core\Config\ImmutableConfig $systemDateConfig
   *   The system date config.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cacheTagsInvalidator
   *   The cachetags invalidator.
   */
  public function __construct(
    ImmutableConfig $config,
    ImmutableConfig $systemDateConfig,
    CacheTagsInvalidatorInterface $cacheTagsInvalidator
  ) {
    $this->config = $config;
    $this->systemDateConfig = $systemDateConfig;
    $this->cacheTagsInvalidator = $cacheTagsInvalidator;
  }

  /**
   * Hooks into presave for a term object.
   *
   * Sets published when set publish date has passed when
   * fields are present and filled.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityPresaveEvent $event
   *   The event.
   */
  public function termPresave(EntityPresaveEvent $event): void {
    $entity = $event->getEntity();

    if (!$entity instanceof TermInterface) {
      return;
    }

    $vocabularies = $this->config->get('vocabularies');

    if ($vocabularies === NULL) {
      return;
    }

    $fieldName = $this->config->get('field_name');

    if (empty($fieldName)) {
      return;
    }

    if (!\in_array($entity->bundle(), $vocabularies, TRUE)) {
      return;
    }

    if (!$entity->hasField($fieldName)) {
      return;
    }

    if ($entity->get($fieldName)->isEmpty()) {
      return;
    }

    $fieldValue = $entity->get($fieldName);

    if (!isset($fieldValue->date)) {
      return;
    }

    $date = $fieldValue->date;

    if (!$date instanceof DrupalDateTime) {
      return;
    }

    $setUnixTime = \DateTimeImmutable::createFromFormat(
      'Y-m-d\TH:i:s',
      $date->format('Y-m-d\TH:i:s', ['timezone' => 'UTC']),
      new \DateTimeZone($this->systemDateConfig->get('timezone.default')))->format('U');

    if ($setUnixTime <= time()) {
      $entity->setPublished();
      $this->cacheTagsInvalidator->invalidateTags($entity->getCacheTags());
      return;
    }

    $entity->setUnpublished();
    $this->cacheTagsInvalidator->invalidateTags($entity->getCacheTags());
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      EntityHookEvents::ENTITY_PRE_SAVE => 'termPresave',
    ];
  }

}
