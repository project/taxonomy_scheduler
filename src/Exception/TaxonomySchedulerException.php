<?php

namespace Drupal\taxonomy_scheduler\Exception;

/**
 * Class TaxonomySchedulerException.
 *
 * Exception wrapper.
 */
final class TaxonomySchedulerException extends \RuntimeException {}
